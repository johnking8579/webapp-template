package my.web;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import my.common.SignValid;

public class SignInterceptor implements HandlerInterceptor  {
	
	private boolean ignoreSign;
	
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object obj) throws Exception {
		if(obj instanceof HandlerMethod) {
			if(((HandlerMethod)obj).getMethodAnnotation(NeedSign.class) != null) {
				if(ignoreSign)	return true;
				boolean valid = SignValid.valid(req.getParameterMap());
				if(!valid) {
					PrintWriter pw = resp.getWriter();
					pw.write(MyExceptionHandler.handleApiException(new SignException()));
					pw.flush();
					pw.close();
				}
				return valid;
			}
		}
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse arg1, Object arg2, ModelAndView mav)
			throws Exception {
	}
	

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}

	public void setIgnoreSign(boolean ignoreSign) {
		this.ignoreSign = ignoreSign;
	}
}
