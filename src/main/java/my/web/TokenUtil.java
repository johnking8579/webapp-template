package my.web;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class TokenUtil {
	
	private static final String PWD = "J1t4QbNDTSGIaRQVa9exqaSbeEGzQ00hiGkUFWvSBITwufRExHdIGLG9qlcZJjCy";
	
	public static void main(String[] args) throws IllegalTokenException {
		byte[] b = AESEncrypt("4".getBytes(), PWD);
		String s = Base64.encodeBase64URLSafeString(b);
		System.out.println(s);
		System.out.println(parse(s));
	}
	public static String gen(String id) {
		String s = id+"|"+System.currentTimeMillis();
		byte[] b;
		try {
			b = AESEncrypt(s.getBytes("utf-8"), PWD);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		return Base64.encodeBase64URLSafeString(b);
	}
	
	public static int parse(String token) throws IllegalTokenException {
		try {
			byte[] b = Base64.decodeBase64(token);
			b = AESDecrypt(b, PWD);
			String s= new String(b);
			return Integer.parseInt(s.split("\\|")[0]);
		} catch (Exception e) {
			throw new IllegalTokenException(e);
		}
	}
	
	 static byte[] AESEncrypt(byte[] original, String seed) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(seed.getBytes()));
            SecretKeySpec key = new SecretKeySpec(kgen.generateKey().getEncoded(), "AES");// 转换为AES专用密钥
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(original);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
     }
		 
	 static byte[] AESDecrypt(byte[] encrypted, String seed) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES"); 
            kgen.init(128, new SecureRandom(seed.getBytes()));
            SecretKeySpec key = new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"); 
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key); 
            return cipher.doFinal(encrypted);  
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
