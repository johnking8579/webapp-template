package my.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * 得到request对应的用户, 放到controller的方法参数上
 * 
 * <mvc:annotation-driven>
	 	<!-- 注册UserBind到方法参数上 -->
		 <mvc:argument-resolvers>
	    	<bean class="com.ted.bh.web.auth.UserBindArgResolver">
	    		<property name="securityManager" ref="securityManager"/>
	    	</bean>
	    </mvc:argument-resolvers> 
	    
		<!-- @ResponseBody时使用utf-8 
		<mvc:message-converters register-defaults="true">
		 <bean class="org.springframework.http.converter.StringHttpMessageConverter">
	      	<property name="supportedMediaTypes">
	      		<list>
					<value>text/html;charset=UTF-8</value>
	      		</list>
	      	</property>
	      	<property name="writeAcceptCharset" value="false"/>
	    </bean>
		</mvc:message-converters>
		-->
	</mvc:annotation-driven>
	
	
	@RequestMapping("/")
	@ResponseBody
	public String methodInController(@UserBind User user) {}
 */
public class TokenArgResolver implements HandlerMethodArgumentResolver{
	
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(Token.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {

       HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
       //HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
       String paramKey = parameter.getParameterAnnotation(Token.class).value();
       String token = request.getParameter(paramKey);
	   return TokenUtil.parse(token);
    }
}