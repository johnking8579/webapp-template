package my.web.ctrl;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import my.web.Out;
import my.web.MyExceptionHandler;
import my.web.NeedSign;

@Controller
@RequestMapping("passport")
@ResponseBody
public class PassportController {
	Gson gson = new GsonBuilder().serializeNulls().create();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@RequestMapping(value="login", method=RequestMethod.POST)
	@NeedSign
	public String login(HttpServletRequest request, @RequestParam String imei) {
		return new Out().toString();
	}
	
	@ExceptionHandler
	@ResponseBody
	public String handleException(Exception e) {
		return MyExceptionHandler.handleApiException(e);
	}

}
