package my.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;

import my.common.Pager;

public class BaseDao<T> {

	protected JdbcTemplate jdbcTemplate;
	protected HibernateTemplate hibernateTemplate;
	
	@Resource
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void persist(Object o) {
		hibernateTemplate.persist(o);
	}

	public void delete(Object o) {
		hibernateTemplate.delete(o);
	}
	
	public void delete(Collection<T> c) {
		hibernateTemplate.deleteAll(c);
	}

	public void flush() {
		hibernateTemplate.flush();
	}

	/**
	 * 查找第一个
	 * 
	 * @param hql
	 * @param values
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected T findLimit1(final String hql, final Map<String,Object> values) {
		List list = (List) hibernateTemplate.execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException {
				Query query = session.createQuery(hql);
				for (Entry<String, Object> e : values.entrySet()) {
					if (e.getValue() instanceof Collection) {
						query.setParameterList(e.getKey(), (Collection) e.getValue());
					} else if (e.getValue() instanceof Object[]) {
						query.setParameterList(e.getKey(), (Object[]) e.getValue());
					} else {
						query.setParameter(e.getKey(), e.getValue());
					}
				}
				return query.setFirstResult(0).setMaxResults(1).list();
			}
		});
		return list.isEmpty() ? null : (T) list.get(0);
	}

	/**
	 * 分页查询
	 * 
	 * @param sql
	 * @param countSql
	 *            页数SQL
	 * @param param
	 *            namedQuery式查询参数
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	protected Pager<T> queryPager(String sql, String countSql, final Map<String, Object> param, final int offset,
			final int pageSize) {
		return new Pager<T>(queryList(sql, param, offset, pageSize), queryInt(countSql, param));
	}

	/**
	 * hql简单分页查询, 使用namedQuery式查询参数 hql中不能带distinct.
	 * 有些比较复杂的hql也不能使用这种方式,需要自己写分页查询语句
	 * 
	 * @param hql
	 * @param param
	 *            key=占位符名称, value=值(不支持基本类型数组)
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	protected Pager<T> queryPager(String sql, final Map<String, Object> param, final int offset, final int pageSize) {
		return queryPager(sql, toCountSql(sql), param, offset, pageSize);
	}

	/**
	 * 把简单的sql查询语句转换成count语句.不适用于复杂sql.
	 * 
	 * @param sql
	 * @return
	 */
	protected String toCountSql(String sql) {
		String regex = "(?=.* (?i:join|distinct)).*";
		if (sql.matches(regex))  throw new IllegalArgumentException("sql不能包含这些关键字:"+regex);
		return sql
				.replaceAll(".*from ", "select count(*) from ")
				.replaceAll("order by .*", "");
	}
	public static void main(String[] args) {
		String sql = "select (a) from tableJOIN a  where 1=1";
		System.out.println(sql.matches("(?=.* (?i:join|distinct)).*"));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected List<T> queryList(final String sql, final Map<String, Object> param, final int offset,
			final int pageSize) {
		return (List<T>) hibernateTemplate.execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session) throws HibernateException {
				Query query = session.createQuery(sql);
				for (Entry<String, Object> e : param.entrySet()) {
					if (e.getValue() instanceof Collection) {
						query.setParameterList(e.getKey(), (Collection) e.getValue());
					} else if (e.getValue() instanceof Object[]) {
						query.setParameterList(e.getKey(), (Object[]) e.getValue());
					} else {
						query.setParameter(e.getKey(), e.getValue());
					}
				}
				return query.setFirstResult(offset).setMaxResults(pageSize).list();
			}
		});
	}

	/**
	 * 根据count语句,得到个数
	 * 
	 * @param countHql
	 * @param values
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected int queryInt(final String sql, final Map<String, Object> param) {
		List list = (List) getHibernateTemplate().execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException {
				Query query = session.createQuery(sql);
				for (Entry<String, Object> e : param.entrySet()) {
					if (e.getValue() instanceof Collection) {
						query.setParameterList(e.getKey(), (Collection) e.getValue());
					} else if (e.getValue() instanceof Object[]) {
						query.setParameterList(e.getKey(), (Object[]) e.getValue());
					} else {
						query.setParameter(e.getKey(), e.getValue());
					}
				}
				return query.list();
			}
		});
		return list.isEmpty() ? 0 : ((Long) list.get(0)).intValue();
	}

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	protected HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	protected Map<String, Object> genParam(Object... objects) {
		Map<String, Object> param = new HashMap<>();
		if (objects != null) {
			for (int i = 1; i <= objects.length; i++) {
				param.put("p" + i, objects[i - 1]);
			}
		}
		return param;
	}
	
	protected Pager<T> jdbcQueryPager(String sql, String pageSql, List<Object> param, int offset, int pageSize, RowMapper<T> rowMapper) {
		Object[] par = param.toArray(new Object[]{});
		long count = jdbcTemplate.queryForObject(pageSql, par, Long.class);
		sql += " LIMIT ?, ?";
		param.add(offset);
		param.add(pageSize);
		List<T> list = jdbcTemplate.query(sql, param.toArray(new Object[]{}), rowMapper);
		return new Pager<T>(list, count);
	}

}
