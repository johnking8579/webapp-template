package my.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * properties文件加载类, 从spring文件中查找需要加载的properites. 
 * 不采用注入配置文件的方式初始化,因为特殊情况下会在spring启动前使用Conf
 */
public class Conf {
	
	private static final String XML = "applicationContext.xml";
	
	public static String getexp_validate_url() {
		return get("exp_validate_url");
	}
	public static String[] getServerUrl() {
		String serverUrl = get("server_url");
		if(serverUrl!=null&&!serverUrl.isEmpty()){
			return serverUrl.split(";");
		}
		return null;
	}

	public static String get(String key)	{
		return instance.getProperty(key);
	}
	
	public static String get(String key, String defaultValue)	{
		return instance.getProperty(key, defaultValue);
	}
	
	private static Properties instance;
	static {
		String prop = choosePropFile();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(prop.replace("classpath:", "").trim());
		if(is == null)	
			throw new ApplicationException("没有找到关键配置文件 classpath:" + prop);
		
		instance = new Properties();
		try {
			instance.load(is);
		} catch (IOException e) {
			throw new ApplicationException(e);
		} finally	{
			Util.closeStream(is);
		}
	}
	
	/**
	 * 从springXml查找需要加载的properties
	 * @return
	 */
	private static String choosePropFile()	{
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(XML);
		if(is == null)
			throw new ApplicationException("没有找到关键配置文件 classpath:" + XML);
		try {
			Element root = new SAXReader().read(is).getRootElement();
			String propFile = null;
			for(Element bean : (List<Element>)root.elements("bean"))	{
				if("org.springframework.beans.factory.config.PropertyPlaceholderConfigurer".equals(bean.attributeValue("class")))	{
					for(Element prop : (List<Element>)bean.elements("property"))	{
						if("location".equals(prop.attributeValue("name")))	{
							propFile = prop.elementText("value");
						}
					}
				}
			}
			if(propFile == null)
				throw new ApplicationException("在spring配置文件中没有查到properties文件");
			return propFile;
		} catch (DocumentException e) {
			throw new ApplicationException(e);
		} finally	{
			Util.closeStream(is);
		}
	}
}
