package my.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class SignGen {
	
	public static void main(String[] args) {
		Map<String,String> par = new HashMap<>();
//		par.put("b", "二");
//		par.put("c", "三");
//		par.put("a", "一");
		par.put("uid", "33");
		
		String sign = SignGen.genSign(par);
		System.out.println("提交到服务器的参数:" + paramMapToString(par, "utf-8") + "&sign=" + sign);
	}
	
	/**
	 * 生成url签名值
	 * 注1:value值为urlEncoder之前的原始值
	 * 注2:key值不能重复
	 * @param param 
	 * @return
	 */
	public static String genSign(Map<String,String> param) {
		if(!(param instanceof TreeMap)) {
			param = new TreeMap<>(param);
		}
		String s = "4c20aadd9896435" + paramMapToString(param, null) + "385c6454939a92b38";
		try {
			return toMd5(s.getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static String paramMapToString(Map paramMap, String charset)	{
		if(paramMap.size() == 0)	return "";
		StringBuilder sb = new StringBuilder();
		for(Object o : paramMap.entrySet())	{
			Entry e = (Entry)o;
			if(e.getValue() instanceof String[])	{
				for(String val : (String[])e.getValue())	{
					if(charset != null)	{
						try {
							val = URLEncoder.encode(val, charset);
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
						}
					}
					sb.append(e.getKey()).append("=").append(val).append("&");
				}
			} else {
				String value = (String)e.getValue();
				if(charset != null)	{
					try {
						value = URLEncoder.encode(value, charset);
					} catch (UnsupportedEncodingException e1) {
						e1.printStackTrace();
					}
				}
				sb.append(e.getKey()).append("=").append(value).append("&");
			}
		}
		if(sb.toString().endsWith("&"))	{
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
	
	private static String toMd5(byte[] cont){  
	    try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        md.update(cont);  
	        byte[] byteDigest = md.digest();  
	        int i;  
	        StringBuilder buf = new StringBuilder();  
	        for (int offset = 0; offset < byteDigest.length; offset++) {  
	            i = byteDigest[offset];  
	            if (i < 0)  i += 256;  
	            if (i < 16) buf.append("0"); 
	            buf.append(Integer.toHexString(i));
	        }
//	        return buf.toString().substring(8, 24);		//16位加密     
	        return buf.toString();						//32位加密    
	    } catch (NoSuchAlgorithmException e) {  
	        throw new RuntimeException(e); 
		}
	}

}
