package my.common.http.log;

/**
 * PacketLogParser的简单实现类.
 * 不记录请求体, 只记录响应体, 将响应体以utf8编码写入日志
 */
public class Utf8RespLogParser implements PacketLogParser {

	@Override
	public String getServiceName() {
		return null;
	}

	@Override
	public String parseRequest(byte[] request) throws Exception {
		return null;
	}

	@Override
	public String parseResponse(byte[] response) throws Exception {
		if(response == null)	return null;
		return new String(response, "utf-8");
	}

}
