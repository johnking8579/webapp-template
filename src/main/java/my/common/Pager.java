package my.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 简单分页模型
 */
public class Pager<T> {

	protected List<T> data = new ArrayList<T>();
	protected long total;

	public Pager() {
	}

	public Pager(List<T> data, long total) {
		this.data = data;
		this.total = total;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

}
